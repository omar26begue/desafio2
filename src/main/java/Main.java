import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.previred.desafio.tres.uf.DatosUf;
import com.previred.desafio.tres.uf.Valores;
import com.previred.desafio.tres.uf.vo.Uf;
import com.previred.desafio.tres.uf.vo.Ufs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static String PARAM_FILE_OUT = "response.json";
    private static String PARAM_FORMAT_DATE = "yyyy-MM-dd";
    private static FileOutputStream outputStream;

    public static void main(String[] args) throws IOException {
        Logger logger = Logger.getLogger("Desafio 3");
        //FileOutputStream outputStream = new FileOutputStream(PARAM_FILE_OUT);;

        try {
            Valores valores = new Valores();

            // obteniendo listado de valores UF
            Ufs rango = valores.getRango();
            List<Uf> ufs = DatosUf.getInstance().getUfs(rango.getInicio(), rango.getFin());
            if (ufs.size() == 0) {
                throw new Exception("No existen valores para el rando de fechas " + rango.getInicio().toString() + " " + rango.getFin().toString());
            }
            // ordenando el listado de valores
            ufs.sort(new Comparator<Uf>() {
                @Override
                public int compare(Uf o1, Uf o2) {
                    return o1.getFecha().after(o2.getFecha()) ? -1 : 1;
                }
            });

            ResponseJSON json = new ResponseJSON(rango.getInicio(), rango.getFin(), new ArrayList<>());
            for (int i = 0; i < ufs.size(); i++) {
                json.getUFs().add(new ResponseUf(ufs.get(i).getFecha(), ufs.get(i).getValor().toString()));
            }

            // creando la carpeta de respuesta
            Path folder = Paths.get("").toAbsolutePath().normalize();
            outputStream = new FileOutputStream(PARAM_FILE_OUT);

            // formato de fecha en el archivo JSON
            Gson gson = new GsonBuilder().setDateFormat(PARAM_FORMAT_DATE).create();

            // creación del archivo de respuesta
            Path target = folder.resolve(PARAM_FILE_OUT);
            outputStream = new FileOutputStream(target.toString());
            logger.log(Level.INFO, "Creando archivo " + PARAM_FILE_OUT);
            outputStream.write(gson.toJson(json).getBytes(StandardCharsets.UTF_8));

            // mensaje de finalizado
            logger.log(Level.INFO, "Finalizado !!!!");
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
        } finally {
            outputStream.close();
        }
    }
}
